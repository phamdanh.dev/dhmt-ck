package CK;

import java.awt.Color;

/*  bezmesh.java
*  This program renders a lighted, filled Bezier surface,
*  using two-dimensional evaluators.
*/

import java.awt.Frame;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import jgl.GL;
import jgl.GLCanvas;
import jgl.GLUT;

public class NewBoat extends GLCanvas {

	private static final float ctrlpoints[][][] = {
			{ { 0f, 3f, 0f }, { 0f, 3f, 0f }, { 0f, 3f, 0f }, { 0f, 3f, 0f } },
			{ { -1.5f, 2.5f, 0f }, { -1.5f, 2.5f, -1.5f }, { 1.5f, 2.5f, -1.5f }, { 1.5f, 2.5f, 0f } },
			{ { -1.5f, -2.5f, 0f }, { -1.5f, -2.5f, -1.5f }, { 1.5f, -2.5f, -1.5f }, { 1.5f, -2.5f, 0f } },
			{ { 0f, -3f, 0f }, { 0f, -3f, 0f }, { 0f, -3f, 0f }, { 0f, -3f, 0f } } };

	private static float rotate = -100, x = 1, y = 0.7f, xTranslate = 0.0f;

	private void rotateTop() {
//		rotate = rotate + 5;
//		x = 1;
//		y = 0;
	}

	private void rotateBottom() {
//		rotate = rotate - 5;
//		x = 1;
//		y = 0;
	}
	
	private void rotateLeft() {
//		rotate = rotate + 5;
//		x = 0;
//		y = 1;
		y = y + 0.1f;
	}

	private void rotateRight() {
//		rotate = rotate - 5;
//		x = 0;
//		y = 1;
		y = y - 0.1f;
	}
	
	
	float nAnimate = 0.00f;
	int flagAnimate = 0;
	public void animate(int value) throws InterruptedException {
		
		if (flagAnimate == 0) {
			nAnimate += 0.002;
			y = y + nAnimate;
			if (nAnimate >= 0.03) {
				flagAnimate = 1;
				nAnimate = 0;
			}
			xTranslate += 0.02f;
//			display();
//			repaint();
//			return;
		}
		if (flagAnimate == 1) {
			nAnimate += 0.002;
			y = y - nAnimate;
			if (nAnimate >= 0.03) {
				flagAnimate = 0;
				nAnimate = 0;
			}
			xTranslate -= 0.02f;
//			display();
//			repaint();
//			return;
		}
		display();
		myUT.glutPostRedisplay();
		myUT.glutTimerFunc(value, "animate", value);
			
	}
	
	
	/* Create checkerboard texture */
	private static final int checkImageWidth = 256;
	private static final int checkImageHeight = 256;
	private byte imageCenter[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageRight[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageLeft[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageTop[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageBottom[][][] = new byte[checkImageWidth][checkImageHeight][4];

	private int texName[] = new int[5];

	private void makeCheckImage() throws IOException {
		// Tao file va Buffer
		File bmpCenterFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\BedRoom_Center.bmp");
		File bmpRightFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\BedRoom_Right.bmp");
		File bmpLeftFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\BedRoom_Left.bmp");
		File bmpTopFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\BedRoom_Top.bmp");
		File bmpBottomFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\watercopy.bmp");
		BufferedImage iCenter = ImageIO.read(bmpCenterFile);
		BufferedImage iRight = ImageIO.read(bmpRightFile);
		BufferedImage iLeft = ImageIO.read(bmpLeftFile);
		BufferedImage iTop = ImageIO.read(bmpTopFile);
		BufferedImage iBottom = ImageIO.read(bmpBottomFile);

		for (int i = 0; i < checkImageWidth; i++) {
			for (int j = 0; j < checkImageHeight; j++) {
				Color cCenter = new Color(iCenter.getRGB(i, j));
				imageCenter[j][i][0] = (byte) (cCenter.getRed());
				imageCenter[j][i][1] = (byte) (cCenter.getGreen());
				imageCenter[j][i][2] = (byte) (cCenter.getBlue());
				imageCenter[i][j][3] = (byte) 255;
				
				Color cRight = new Color(iRight.getRGB(i, j));
				imageRight[j][i][0] = (byte) (cRight.getRed());
				imageRight[j][i][1] = (byte) (cRight.getGreen());
				imageRight[j][i][2] = (byte) (cRight.getBlue());
				imageRight[i][j][3] = (byte) 255;
				
				Color cLeft = new Color(iLeft.getRGB(i, j));
				imageLeft[j][i][0] = (byte) (cLeft.getRed());
				imageLeft[j][i][1] = (byte) (cLeft.getGreen());
				imageLeft[j][i][2] = (byte) (cLeft.getBlue());
				imageLeft[i][j][3] = (byte) 255;
				
				Color cTop = new Color(iTop.getRGB(i, j));
				imageTop[j][i][0] = (byte) (cTop.getRed());
				imageTop[j][i][1] = (byte) (cTop.getGreen());
				imageTop[j][i][2] = (byte) (cTop.getBlue());
				imageTop[i][j][3] = (byte) 255;
				
				Color cBottom = new Color(iBottom.getRGB(i, j));
				imageBottom[j][i][0] = (byte) (cBottom.getRed());
				imageBottom[j][i][1] = (byte) (cBottom.getGreen());
				imageBottom[j][i][2] = (byte) (cBottom.getBlue());
				imageBottom[i][j][3] = (byte) 255;
			}
		}
	}

	public void display() {
		myGL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		
		//BACKGROUND
		displayBackground();
		//END BACKGROUND
		drawClear();
		//BOAT
		displayBoat();
		//END BOAT

		
		myGL.glFlush();
	}
	
	 

	private void myinit() throws IOException {
//		initlights();


	
		//BACKGROUND
		myGL.glPushMatrix();
		initBackground();
		myGL.glPopMatrix();
		//END BACKGROUND
		
		
		//BOAT
		myGL.glPushMatrix();
		initBoat();
		myGL.glPopMatrix();
		//END BOAT

		
	}
	
	
	
	public void displayBoat() {
		float ambient[] = { 131.0f/255.0f, 84.0f/255.0f, 38.0f/255.0f, 1.0f };
		float diffuse[] = { 131.0f/255.0f, 84.0f/255.0f, 38.0f/255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = {57.0f/255.0f, 36.0f/255.0f, 15.0f/255.0f, 1.0f};
		float shininess[] = { 20 };
		

		myGL.glPushMatrix();
//		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
//		myGL.glPushMatrix();
		myGL.glScalef(0.3f, 0.3f, 0.3f);
		myGL.glRotatef(-10, 1.0f, 0.0f, 0.0f);
		myGL.glRotatef((float) rotate, x, y, 1.0f); // 85, 1,1,1
		myGL.glTranslatef( 2.5f, -1.0f, -2.4f);
		myGL.glTranslatef( 0, 0, xTranslate);
		myGL.glEvalMesh2(GL.GL_FILL, 0, 20, 0, 20);
		myGL.glPopMatrix();
	}
	
	public void drawClear() {
		float mat_ambient[] = { 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 0.1f };
		float mat_diffuse[] = { 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 0.1f };
		float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float low_shininess[] = { 5.0f };
		float mat_emission[] = { 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 0.1f };
		myGL.glPushMatrix();
		myGL.glTranslated(-0.25, -2.2, 0.8);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, mat_ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, low_shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, mat_emission);
		myGL.glPopMatrix();
	}
	
	public void displayBackground() {
		myGL.glPushMatrix();
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[0]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-1.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(1.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(1.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-1.0f, 1.0f, 0.0f);
		myGL.glEnd();
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[1]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(1.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(1.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(1.0f, 1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(1.0f, 1.0f, 0.0f);
		myGL.glEnd();
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[2]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-1.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(-1.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(-1.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-1.0f, 1.0f, 2.0f);
		myGL.glEnd();
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[3]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-1.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(1.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(1.0f, 1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-1.0f, 1.0f, 2.0f);
		myGL.glEnd();
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[4]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-1.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(1.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(1.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-1.0f, -1.0f, 0.0f);
		myGL.glEnd();
		
		myGL.glPopMatrix();
	}

	
	

	public void init() throws IOException {
		myUT.glutInitWindowSize(500, 500);
		myUT.glutInitWindowPosition(0, 0);
		myUT.glutCreateWindow(this);
		myinit();
		myUT.glutReshapeFunc("myReshape");
		myUT.glutDisplayFunc("display");
		myUT.glutKeyboardFunc("keyboard");
		myUT.glutMouseFunc("mouse");
		myUT.glutMainLoop();
	}
	private void initlights () {
		float ambient[] = {0.0f, 0.0f, 0.0f, 0.0f};
		float position[] = {50.0f, -50.0f, 40.0f, 1.0f};// 0, -2, 4
		float mat_diffuse[] = {0.6f, 0.6f, 0.6f, 1.0f};
		float mat_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	        float mat_shininess[] = {50.0f};

	        myGL.glEnable (GL.GL_LIGHTING);
	        myGL.glEnable (GL.GL_LIGHT0);

	        myGL.glLightfv (GL.GL_LIGHT0, GL.GL_AMBIENT, ambient);
	        myGL.glLightfv (GL.GL_LIGHT0, GL.GL_POSITION, position);

//	        myGL.glMaterialfv (GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
//	        myGL.glMaterialfv (GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
//	        myGL.glMaterialfv (GL.GL_FRONT, GL.GL_SHININESS, mat_shininess);
	    }
	public void initBoat() {
		myGL.glPushMatrix();
		initlights();
		myGL.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		myGL.glEnable(GL.GL_DEPTH_TEST);
		myGL.glMap2f(GL.GL_MAP2_VERTEX_3, 0.0f, 1.0f, 3, 4, 0.0f, 1.0f, 12, 4, ctrlpoints);
		myGL.glEnable(GL.GL_MAP2_VERTEX_3);
		myGL.glEnable(GL.GL_AUTO_NORMAL);
		myGL.glEnable(GL.GL_NORMALIZE);
		myGL.glMapGrid2f(20, 0.0f, 1.0f, 20, 0.0f, 1.0f);
		myGL.glPopMatrix();
	}
	public void initBackground() throws IOException {
		myGL.glPushMatrix();
		makeCheckImage();
		myGL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 1);

		myGL.glGenTextures(5, texName);

		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[0]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageCenter);

		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[1]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageRight);
		myGL.glEnable(GL.GL_TEXTURE_2D);
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[2]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageLeft);
		myGL.glEnable(GL.GL_TEXTURE_2D);
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[3]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageTop);
		myGL.glEnable(GL.GL_TEXTURE_2D);
//		myGL.glDisable(GL.GL_TEXTURE_2D);
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[4]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageBottom);
		myGL.glEnable(GL.GL_TEXTURE_2D);
		myGL.glPopMatrix();
//		myGL.glDisable(GL.GL_TEXTURE_2D);
	}

	
	public void myReshape(int w, int h) {
		myGL.glViewport(0, 0, w, h);
		myGL.glMatrixMode(GL.GL_PROJECTION);
		myGL.glLoadIdentity();
//		if (w <= h) {
//			myGL.glOrtho(-4.0f, 4.0f, -4.0f * (float) h / (float) w, 4.0f * (float) h / (float) w, -4.0f, 4.0f);
//		} else {
//			myGL.glOrtho(-4.0f * (float) w / (float) h, 4.0f * (float) w / (float) h, -4.0f, 4.0f, -4.0f, 4.0f);
//		}
		myGLU.gluPerspective(60.0, 1.0 * (double) w / (double) h, 0.1, 30.0);
		myGL.glMatrixMode(GL.GL_MODELVIEW);
		myGL.glLoadIdentity();
		myGL.glTranslatef(0.0f, 0.0f, -3.6f);
	}
	public void keyboard(char key, int x, int y) throws InterruptedException {
		switch (key) {
		case 27:
			System.exit(0);
		case ' ':
			System.out.println("SPACE");
			animate(0);
			break;
		case 'a':
			System.out.println("A");
			rotateLeft();
			display();
			repaint();
			break;
		case 'd':
			System.out.println("D");
			rotateRight();
			display();
			repaint();
			break;
		default:
			break;
		}
	}
	
	public void mouse(int button, int state, int x, int y) throws InterruptedException {
		if (button == GLUT.GLUT_LEFT_BUTTON) {
			if (state == GLUT.GLUT_DOWN) {
				myUT.glutTimerFunc(20, "animate", 20);

			}
		} else if (button == GLUT.GLUT_MIDDLE_BUTTON) {
			if (state == GLUT.GLUT_DOWN) {
				myUT.glutIdleFunc(null);
			}
		}
	}
	
	static public void main(String args[]) throws IOException {
		Frame mainFrame = new Frame();
		mainFrame.setSize(508, 527);
		NewBoat mainCanvas = new NewBoat();
		mainCanvas.init();
		mainFrame.add(mainCanvas);
		mainFrame.setVisible(true);
	}

}

package CK;

import java.awt.Color;

/*
 *  doublebuffer.java
 *  This is a simple double buffered program.
 *  Pressing the left mouse button rotates the rectangle.
 *  Pressing the middle mouse button stops the rotation.
 */

import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.String;
import java.lang.System;

import javax.imageio.ImageIO;

import jgl.GL;
import jgl.GLAUX;
import jgl.GLUT;
import jgl.GLCanvas;
import jgl.GLU;

// "double" is a reserved word in Java
public class RobotGLCanvas extends GLCanvas {
	
	GLAUX myAUX = new GLAUX(myGL);
	
	private static float shoulderLeft = 0, elbowLeft = 0, shoulderRight = 0, elbowRight = 0, legLeft = 0, legRight = 0,
			look = 0, xLeg = 0, zLeg = 0, cangTrai = 0, cangPhai = 0, rTranslateX = 0, rTranslateZ = 0, bTranslateX = 0,
			bTranslateY = 0, bTranslateZ = 0;
	
	
	
	
	/*
	 * 
	 * 
	 * BACKGROUND
	 * 
	 * 
	 * 
	 */
	
	/* Create checkerboard texture */
	private static final int checkImageWidth = 256;
	private static final int checkImageHeight = 256;
	private byte imageCenter1[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageCenter2[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageRight[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageLeft[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageTop[][][] = new byte[checkImageWidth][checkImageHeight][4];
	private byte imageBottom[][][] = new byte[checkImageWidth][checkImageHeight][4];

	private int texName[] = new int[6];

	private void makeCheckImage() throws IOException {
		// Tao file va Buffer
		File bmpCenter1File = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\poolcen1.bmp");
		File bmpCenter2File = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\poolcen2.bmp");
		File bmpRightFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\poolright.bmp");
		File bmpLeftFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\poolleft.bmp");
		File bmpTopFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\pooltop.bmp");
		File bmpBottomFile = new File("D:\\C� nh�n\\HK1-2021\\DHMT\\tham-khao-them\\Images\\poolbottom.bmp");
		BufferedImage iCenter1 = ImageIO.read(bmpCenter1File);
		BufferedImage iCenter2 = ImageIO.read(bmpCenter2File);
		BufferedImage iRight = ImageIO.read(bmpRightFile);
		BufferedImage iLeft = ImageIO.read(bmpLeftFile);
		BufferedImage iTop = ImageIO.read(bmpTopFile);
		BufferedImage iBottom = ImageIO.read(bmpBottomFile);

		for (int i = 0; i < checkImageWidth; i++) {
			for (int j = 0; j < checkImageHeight; j++) {
				Color cCenter1 = new Color(iCenter1.getRGB(i, j));
				imageCenter1[j][i][0] = (byte) (cCenter1.getRed());
				imageCenter1[j][i][1] = (byte) (cCenter1.getGreen());
				imageCenter1[j][i][2] = (byte) (cCenter1.getBlue());
				imageCenter1[i][j][3] = (byte) 255;
				
				Color cCenter2 = new Color(iCenter2.getRGB(i, j));
				imageCenter2[j][i][0] = (byte) (cCenter2.getRed());
				imageCenter2[j][i][1] = (byte) (cCenter2.getGreen());
				imageCenter2[j][i][2] = (byte) (cCenter2.getBlue());
				imageCenter2[i][j][3] = (byte) 255;
				
				Color cRight = new Color(iRight.getRGB(i, j));
				imageRight[j][i][0] = (byte) (cRight.getRed());
				imageRight[j][i][1] = (byte) (cRight.getGreen());
				imageRight[j][i][2] = (byte) (cRight.getBlue());
				imageRight[i][j][3] = (byte) 255;
				
				Color cLeft = new Color(iLeft.getRGB(i, j));
				imageLeft[j][i][0] = (byte) (cLeft.getRed());
				imageLeft[j][i][1] = (byte) (cLeft.getGreen());
				imageLeft[j][i][2] = (byte) (cLeft.getBlue());
				imageLeft[i][j][3] = (byte) 255;
				
				Color cTop = new Color(iTop.getRGB(i, j));
				imageTop[j][i][0] = (byte) (cTop.getRed());
				imageTop[j][i][1] = (byte) (cTop.getGreen());
				imageTop[j][i][2] = (byte) (cTop.getBlue());
				imageTop[i][j][3] = (byte) 255;
				
				Color cBottom = new Color(iBottom.getRGB(i, j));
				imageBottom[j][i][0] = (byte) (cBottom.getRed());
				imageBottom[j][i][1] = (byte) (cBottom.getGreen());
				imageBottom[j][i][2] = (byte) (cBottom.getBlue());
				imageBottom[i][j][3] = (byte) 255;
			}
		}
	}
	
	
	/*
	 * 
	 * 
	 * 
	 * END BACKGROUND
	 * 
	 * 
	 * 
	 * 
	 */


	

	public void display() {
		
		myGL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT); 
		
		myGL.glPushMatrix();
		myGL.glRotatef(50.0f, 0.0f, 1.0f, 0.0f);
		myGL.glScalef(0.15f, 0.15f, 0.15f);
		myGL.glTranslatef(-0.8f, 0.0f, -2f); //-0.8 , 0.0, -2.0
		myGL.glTranslatef(-3f, -4.0f, 4f); //translate all
		myGL.glTranslatef(rTranslateX, 0.0f, rTranslateZ);
		drawClear();
		drawBody();
		drawClear();
		drawRightArm();
		drawClear();
		drawLeftArm();
		drawClear();
		drawLeftLeg();
		drawClear();
		drawRightLeg();
		drawClear();
		drawHead();
		drawClear();
		myGL.glPopMatrix();
		myGL.glPushMatrix();
		myGL.glScalef(0.15f, 0.15f, 0.15f);
		myGL.glTranslatef(1.3f, -4.0f, 4f); //translate all
		myGL.glTranslatef(bTranslateX, bTranslateY, bTranslateZ);
		drawBall();
		myGL.glPopMatrix();
		drawClear();
//		myGL.glFlush();
		
		/*
		 * 
		 * 
		 * BACKGROUND myGL
		 * 
		 * 
		 */


		
		// BACKGROUND
		myGL.glPushMatrix();
		drawClear();
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[0]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-2.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(0.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(0.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-2.0f, 1.0f, 0.0f);
		myGL.glEnd();
		drawClear();
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[1]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(0.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(2.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(2.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(0.0f, 1.0f, 0.0f);
		myGL.glEnd();
		drawClear();
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[2]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(2.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(2.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(2.0f, 1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(2.0f, 1.0f, 0.0f);
		myGL.glEnd();
		drawClear();
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[3]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-2.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(-2.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(-2.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-2.0f, 1.0f, 2.0f);
		myGL.glEnd();
		drawClear();
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[4]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-2.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(2.0f, 1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(2.0f, 1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-2.0f, 1.0f, 2.0f);
		myGL.glEnd();
		drawClear();
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[5]);
		myGL.glBegin(GL.GL_QUADS);
		myGL.glTexCoord2f(1.0f, 0.0f);myGL.glVertex3f(-2.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(1.0f, 1.0f);myGL.glVertex3f(2.0f, -1.0f, 2.0f);
		myGL.glTexCoord2f(0.0f, 1.0f);myGL.glVertex3f(2.0f, -1.0f, 0.0f);
		myGL.glTexCoord2f(0.0f, 0.0f);myGL.glVertex3f(-2.0f, -1.0f, 0.0f);
		myGL.glEnd();
		drawClear();
		myGL.glPopMatrix();
//		END BACKGROUND
		drawClear();


		
		myGL.glFlush();
		/*
		 * 
		 * 
		 * BACKGROUND myGL
		 * 
		 * 
		 */
		
	}
	
	/*
	 * MY CODE
	 */

	public void drawBody() {
		float ambient[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float diffuse[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };

		myGL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		myGL.glPushMatrix();
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		myAUX.auxSolidBox(0.8, 1.2, 0.5);
		myGL.glPopMatrix();

	}

	public void drawRightArm() {
		float ambient[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float diffuse[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		// end color init
		myGL.glPushMatrix();
		// SHOUDER
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		// end material
		myGL.glTranslated(0.4, 0.5f, 0.0f);
		myGL.glRotatef((float) -60 % 360, 0.0f, 0.0f, 1.0f);
		myGL.glRotatef((float) shoulderRight, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(0.3, 0.0, 0.0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		// ELBOW
		myGL.glPushMatrix();
		float ambient1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f }; // sang
		float specular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission1[] = { 198.0f / 255.0f, 119.0f / 255.0f, 76.0f / 255.0f, 1.0f }; // toi
		float shininess1[] = { 20 };
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission1);
		myGL.glTranslated(0.3, 0.0f, 0.0f);
		myGL.glRotatef((float) elbowRight, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(0.3, 0, 0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		myGL.glPopMatrix();
		// end Elbow

		myGL.glPopMatrix();
	}

	public void drawLeftArm() {
		float ambient[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float diffuse[] = { 20.0f / 255.0f, 190.0f / 255.0f, 186.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		// end color init
		myGL.glPushMatrix();
		// SHOUDER
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		// end material
		myGL.glTranslated(-0.4, 0.5f, 0.0f);
		myGL.glRotatef((float) 60 % 360, 0.0f, 0.0f, 1.0f);
		myGL.glRotatef((float) shoulderLeft, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(-0.3, 0.0, 0.0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		// ELBOW
		myGL.glPushMatrix();
		float ambient1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f }; // sang
		float specular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission1[] = { 198.0f / 255.0f, 119.0f / 255.0f, 76.0f / 255.0f, 1.0f }; // toi
		float shininess1[] = { 20 };
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission1);
		myGL.glTranslated(-0.3, 0.0f, 0.0f);
		myGL.glRotatef((float) elbowLeft, 0.0f, 1.0f, 0.0f);
		myGL.glTranslated(-0.3, 0, 0);
		myAUX.auxSolidBox(0.6, 0.2, 0.2);
		myGL.glPopMatrix();
		// end Elbow
		myGL.glPopMatrix();
	}

	public void drawLeftLeg() {
		float ambient[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 55.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		myGL.glPushMatrix();
		// DUI
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		myGL.glTranslated(-0.25, -0.6f, 0.0f);
		myGL.glRotatef((float) legLeft, xLeg, 0.0f, zLeg);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// CANG CHAN
		myGL.glTranslated(0, -0.4, 0.0);
		myGL.glRotatef((float) cangTrai, 1.0f, 0.0f, 0.0f);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// BAN CHAN
		float ambient2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float diffuse2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float specular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission2[] = { 71.0f / 255.0f, 71.0f / 255.0f, 71.0f / 255.0f, 1.0f };
		float shininess2[] = { 20 };
		myGL.glPushMatrix();
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission2);
		myGL.glTranslated(0, -0.5, 0.1);
		myAUX.auxSolidBox(0.3, 0.2, 0.7);

		myGL.glPopMatrix();
		myGL.glPopMatrix();
	}

	public void drawRightLeg() {
		float ambient[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse[] = { 12.0f / 255.0f, 111.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission[] = { 55.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f, 1.0f };
		float shininess[] = { 20 };
		myGL.glPushMatrix();
		// DUI
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission);
		myGL.glTranslated(0.25, -0.6f, 0.0f);
		myGL.glRotatef((float) legRight, xLeg, 0.0f, zLeg);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// CANG CHAN
		myGL.glTranslated(0, -0.4, 0.0);
		myGL.glRotatef((float) cangPhai, 1.0f, 0.0f, 0.0f);
		myGL.glTranslated(0, -0.4, 0.0);
		myAUX.auxSolidBox(0.3, 0.8, 0.5);
		// BAN CHAN
		float ambient2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float diffuse2[] = { 171.0f / 255.0f, 237.0f / 255.0f, 238.0f / 255.0f, 1.0f };
		float specular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission2[] = { 71.0f / 255.0f, 71.0f / 255.0f, 71.0f / 255.0f, 1.0f };
		float shininess2[] = { 20 };
		myGL.glPushMatrix();
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess2);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission2);
		myGL.glTranslated(0, -0.5, 0.1);
		myAUX.auxSolidBox(0.3, 0.2, 0.7);

		myGL.glPopMatrix();
		myGL.glPopMatrix();
	}

	public void drawHead() {
		myGL.glPushMatrix();
		float ambient1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f };
		float diffuse1[] = { 254.0f / 255.0f, 233.0f / 255.0f, 170.0f / 255.0f, 1.0f }; // sang
		float specular1[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float emission1[] = { 198.0f / 255.0f, 119.0f / 255.0f, 76.0f / 255.0f, 1.0f }; // toi
		float shininess1[] = { 20 };
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, ambient1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, diffuse1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, specular1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, shininess1);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, emission1);
		myGL.glTranslated(0, 0.85, 0);
		myUT.glutSolidSphere(0.25, 50, 50);
		myGL.glPopMatrix();
	}

	public void drawBall() {
		float mat_ambient[] = { 254.0f / 255.0f, 237.0f / 255.0f, 59.0f / 255.0f, 1.0f };
		float mat_diffuse[] = { 254.0f / 255.0f, 237.0f / 255.0f, 59.0f / 255.0f, 1.0f };
		float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float low_shininess[] = { 5.0f };
		float mat_emission[] = { 155.0f / 255.0f, 137.0f / 255.0f, 57.0f / 255.0f, 1.0f };
		myGL.glPushMatrix();
		myGL.glTranslated(-0.25, -2.2, 0.8);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, mat_ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, low_shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, mat_emission);
		myUT.glutSolidSphere(0.25, 50, 50);
		myGL.glPopMatrix();
	}

	public void drawClear() {
		float mat_ambient[] = { 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 0.1f };
		float mat_diffuse[] = { 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 0.1f };
		float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float low_shininess[] = { 5.0f };
		float mat_emission[] = { 255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 0.1f };
		myGL.glPushMatrix();
		myGL.glTranslated(-0.25, -2.2, 0.8);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, mat_ambient);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, low_shininess);
		myGL.glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, mat_emission);
		myGL.glPopMatrix();
	}
	/*
	 * END MY CODE
	 */
	
	private void elbowLeftAdd() {
		elbowLeft = (elbowLeft + 5) % 360;
	}

	private void elbowLeftSubtract() {
		elbowLeft = (elbowLeft - 5) % 360;
	}

	private void shoulderLeftAdd() {
		shoulderLeft = (shoulderLeft + 5) % 360;
	}

	private void shoulderLeftSubtract() {
		shoulderLeft = (shoulderLeft - 5) % 360;
	}

	private void elbowRightAdd() {
		elbowRight = (elbowRight + 5) % 360;
	}

	private void elbowRightSubtract() {
		elbowRight = (elbowRight - 5) % 360;
	}

	private void shoulderRightAdd() {
		shoulderRight = (shoulderRight + 5) % 360;
	}

	private void shoulderRightSubtract() {
		shoulderRight = (shoulderRight - 5) % 360;
	}

	private void legRightAdd() {
		legRight = (legRight + 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	private void legRightSubtract() {
		legRight = (legRight - 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	private void legLeftAdd() {
		legLeft = (legLeft + 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	private void legLeftSubtract() {
		legLeft = (legLeft - 5) % 360;
		xLeg = 0;
		zLeg = 1;
	}

	private void legRightFront() {
		legRight = (legRight + 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void legRightBack() {
		legRight = (legRight - 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void legLeftFront() {
		legLeft = (legLeft + 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void legLeftBack() {
		legLeft = (legLeft - 5) % 360;
		xLeg = 1;
		zLeg = 0;
	}

	private void cangTraiFront() {
		cangTrai = (cangTrai + 5) % 360;
	}

	private void cangTraiBack() {
		cangTrai = (cangTrai - 5) % 360;
	}

	private void cangPhaiFront() {
		cangPhai = (cangPhai + 5) % 360;
	}

	private void cangPhaiBack() {
		cangPhai = (cangPhai - 5) % 360;
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void lookLeft() {
		look = look - 0.1f;
	}

	public void lookRight() {
		look = look + 0.1f;
	}

	// RESET
	public void reset() {
		shoulderLeft = 0;
		elbowLeft = 0;
		shoulderRight = 0;
		elbowRight = 0;
		legLeft = 0;
		legRight = 0;
		look = 0;
		xLeg = 0;
		zLeg = 0;
		cangTrai = 0;
		cangPhai = 0;
		rTranslateX = 0;
		rTranslateZ = 0;
		bTranslateX = 0;
		bTranslateY = 0;
		bTranslateZ = 0;
		countKick = 0;
	}



	// KICK BALL

		int countKick = 0;

		public void kickBall(int value) {
			countKick++;
			if (countKick == 100) {
				return;
			}
			System.out.println(countKick);
			// CHAN TRAI
			if (countKick <= 10) {
				legLeftFront();
				cangTraiFront();
			}
			if (countKick > 10 && countKick <= 30) {
				legLeftBack();
				if (countKick < 21) {
					cangTraiBack();
				}
			}
			if (countKick > 30 && countKick <= 40) {
				legLeftFront();
				legRightBack();
			}
			// END CHAN TRAI

			// CHAN PHAI

			if (countKick <= 10) {
				legRightBack();
				cangPhaiFront();
			}
			if (countKick > 10 && countKick <= 30) {
				legRightFront();
				if (countKick < 21) {
					cangPhaiBack();
				}
			}

			// END CHAN PHAI

			// CHAY
			if (countKick < 40) {
				rTranslateX += 0.01;
				rTranslateZ += 0.04;
			}
			// END CHAY

			// TAY TRAI

			if (countKick <= 10) {
				shoulderLeftAdd();
				elbowLeftAdd();
			}

			if (countKick > 10 && countKick <= 30) {
				shoulderLeftSubtract();
				if (countKick < 22) {
					elbowLeftSubtract();
				}
			}
			if (countKick > 30 && countKick <= 50) {
				shoulderLeftAdd();
				elbowLeftAdd();
			}
			if (countKick > 50 && countKick <= 60) {
				elbowLeftSubtract();
			}
			if (countKick > 60 && countKick <= 70) {
				elbowLeftAdd();
			}
			if (countKick > 70 && countKick <= 80) {
				elbowLeftSubtract();
			}
			if (countKick > 80 && countKick <= 90) {
				elbowLeftAdd();
			}

			// END TAY TRAI

			// TAY PHAI

			if (countKick <= 10) {
				shoulderRightAdd();
			}

			if (countKick > 10 && countKick <= 30) {
				shoulderRightSubtract();
				elbowRightSubtract();
			}
			if (countKick > 50 && countKick <= 60) {
				elbowRightAdd();
			}
			if (countKick > 60 && countKick <= 70) {
				elbowRightSubtract();
			}
			if (countKick > 70 && countKick <= 80) {
				elbowRightAdd();
			}
			if (countKick > 80 && countKick <= 90) {
				elbowRightSubtract();
			}

			// END TAY PHAI

			// BALL
			
			if (countKick > 25) {
				bTranslateX += 0.05;
				bTranslateY += 0.2;
				bTranslateZ += 0.2;
			}

			// END BALL
			display();
			myUT.glutPostRedisplay();
			myUT.glutTimerFunc(value, "kickBall", value);
		}

		// END KICK BALL

	private void myinit() throws IOException {
		myGL.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		myGL.glShadeModel(GL.GL_FLAT);
		float ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
		float diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float position[] = { 0.0f, 3.0f, 2.0f, 0.0f };
		float local_view[] = { 0.0f };

		myGL.glEnable(GL.GL_DEPTH_TEST);
		myGL.glDepthFunc(GL.GL_LESS);

		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, ambient);
		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, diffuse);
		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, specular);
		myGL.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, position);
		myGL.glLightModelfv(GL.GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);

		myGL.glEnable(GL.GL_LIGHTING);
		myGL.glEnable(GL.GL_LIGHT0);

		myGL.glClearColor(0.0f, 0.1f, 0.1f, 0.0f);
		
		/*
		 * 
		 * BACKGROUND
		 */
		
		
		myGL.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		myGL.glShadeModel(GL.GL_FLAT);
		myGL.glEnable(GL.GL_DEPTH_TEST);
		

		myGL.glPushMatrix();
		makeCheckImage();
		myGL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 1);

		myGL.glGenTextures(6, texName);

		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[0]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageCenter1);
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[1]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageCenter2);

		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[2]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageRight);
		myGL.glEnable(GL.GL_TEXTURE_2D);
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[3]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageLeft);
		myGL.glEnable(GL.GL_TEXTURE_2D);
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[4]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageTop);
		myGL.glEnable(GL.GL_TEXTURE_2D);
		
		myGL.glBindTexture(GL.GL_TEXTURE_2D, texName[5]);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		myGL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		myGL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL);
		myGL.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, checkImageWidth, checkImageHeight, 0, GL.GL_RGBA,
				GL.GL_UNSIGNED_BYTE, imageBottom);
		myGL.glEnable(GL.GL_TEXTURE_2D);
		myGL.glPopMatrix();
		
		/*
		 * 
		 * 
		 * END BACKGROUND GL2
		 * 
		 * 
		 */
		
		
		
	}

	public void myReshape(int w, int h) {
		myGL.glViewport(0, 0, w, h);
		myGL.glMatrixMode(GL.GL_PROJECTION);
		myGL.glLoadIdentity();
		myGLU.gluPerspective(60.0, (double) w / (double) h, 0.1, 30.0);
//		myGLU.gluLookAt(look, 0, 0.1, 0, 0, 0, 0, 1, 0);
		myGL.glMatrixMode(GL.GL_MODELVIEW);
		myGL.glLoadIdentity();
		myGL.glTranslatef(0.0f, 0.0f, -3.6f); // 0, 0, -3.6
	
	}

	/* ARGSUSED2 */
	public void mouse(int button, int state, int x, int y) throws InterruptedException {
		if (button == GLUT.GLUT_LEFT_BUTTON) {
			if (state == GLUT.GLUT_DOWN) {
				countKick = 0;
				reset();
				myUT.glutTimerFunc(20, "kickBall", 20);

			}
		} else if (button == GLUT.GLUT_MIDDLE_BUTTON) {
			if (state == GLUT.GLUT_DOWN) {
				myUT.glutIdleFunc(null);
			}
		}
	}

	public void keyboard(char key, int x, int y) {
		switch (key) {
		case 27:
			System.exit(0);
			break;
		case '0':
			reset();
			display();
			myUT.glutTimerFunc(20, null, 0);
			break;
		default:
			break;
		}
	}

	/*
	 * Request double buffer display mode. Register mouse input callback functions
	 */
	public void init() throws IOException {
		myUT.glutInitWindowSize(500, 500);
		myUT.glutInitWindowPosition(0, 0);
		myUT.glutCreateWindow(this);
		myinit();
		myUT.glutDisplayFunc("display");
		myUT.glutReshapeFunc("myReshape");
		myUT.glutMouseFunc("mouse");
		myUT.glutKeyboardFunc("keyboard");
		myUT.glutMainLoop();
	}

	static public void main(String args[]) throws IOException {
		Frame mainFrame = new Frame();
		mainFrame.setSize(508, 527);
		RobotGLCanvas mainCanvas = new RobotGLCanvas();
		mainCanvas.init();
		mainFrame.add(mainCanvas);
		mainFrame.setVisible(true);
	}

}
